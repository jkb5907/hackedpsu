import java.net.URL;
import java.time.format.TextStyle;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @web http://java-buddy.blogspot.com/
 */
public class JavaFXGoogleMaps extends Application {

    public Scene scene;
    MyBrowser myBrowser;
    private ArrayList<Pin> pins;
    private TextArea latInput;
    private  TextArea longInput;
    private CheckBox truck;
    private CheckBox boat;
    private CheckBox medical;
    private TextArea nameInput;
    private TextArea phoneNumberInput;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Stage");
        primaryStage.setWidth(600);
        primaryStage.setHeight(500);
        myBrowser = new MyBrowser();
        String needsString ="";

        Label latLabel = new Label("Latitude");
        latInput = new TextArea("41.4");

        Label longLabel = new Label("Longitude");
        longInput = new TextArea("77.8");

        Label needsLabel = new Label("Needs");
        truck = new CheckBox("Truck");
        boat = new CheckBox("Boat");
        medical = new CheckBox("Medical Attention");

        Label nameLabel = new Label("Name");
        nameInput = new TextArea("Name");

        Label phoneNumberLabel = new Label("Phone Number");
        phoneNumberInput = new TextArea("5555555555");

        Button addMarker = new Button("Add Marker");
        addMarker.setOnAction(new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                addMarker();
            }
        });
        //HBox h = new HBox(myBrowser,addMarker);

        GridPane gridPane = new GridPane();
        gridPane.addRow(0, myBrowser);
        gridPane.addRow(1, nameLabel, nameInput);
        gridPane.addRow(2, phoneNumberLabel, phoneNumberInput);
        gridPane.addRow(3, latLabel, latInput);
        gridPane.addRow(4, longLabel, longInput );
        gridPane.addRow(5, needsLabel, truck, boat, medical);
        gridPane.addRow(6, addMarker);

        scene = new Scene(gridPane, 600, 500);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public void addMarker(){
        //myBrowser.getWebEngine().executeScript("addMarker({lat: -40.8, lng: 77.8})");
        Pin p = new Pin();
        String needsString = "";

        double la = Double.parseDouble(latInput.getText());
        double lo = Double.parseDouble(latInput.getText());

        double[] arr = {la,lo};
        if(truck.isSelected()){
            p.addNeed("Truck");
            needsString+="Truck, ";
        }
        if(boat.isSelected()){
            p.addNeed("Boat");
            needsString+="Boat, ";
        }
        if(medical.isSelected()){
            p.addNeed("Medical Attention");
            needsString+="Medical Attention, ";
        }
        p.setName(nameInput.getText());
        p.setPhoneNumber(phoneNumberInput.getText());

        p.setCoords(arr);

        String scriptPointer = "";
        String pString = p.toString();
        //System.out.println(p);
        scriptPointer = "addMarkerWithContent({lat: "+p.getCoords()[0] +", lng: "+p.getCoords()[1]+"}, '" + p.getName()
                + "','" + p.getPhoneNumber() + "','" + needsString + "')";
        System.out.println(scriptPointer);

        myBrowser.getWebEngine().executeScript(scriptPointer);
        //myBrowser.getWebEngine().executeScript("addMarker({lat: -40.8, lng: 77.8},'Testing')");

    }

    public static void main(String[] args) {
        launch(args);
    }

    public class MyBrowser extends Region {

        public WebView webView = new WebView();
        public WebEngine webEngine = webView.getEngine();

        public MyBrowser() {

            final URL urlGoogleMaps = getClass().getResource("GoogleMapsV3.html");
            System.out.println(urlGoogleMaps);
            webEngine.load(urlGoogleMaps.toExternalForm());
            webEngine.setJavaScriptEnabled(true);

            getChildren().add(webView);
        }

        public WebEngine getWebEngine(){
            return webEngine;
        }

    }
}