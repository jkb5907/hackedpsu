import java.util.ArrayList;

public class Pin {

    private String name;
    private String address;
    private ArrayList<String> needs;
    private double[] coords;
    private String phoneNumber;

    public Pin(){
        name = "test";
        address = "123 Rumford rd.";
        needs = new ArrayList<>();
        coords = new double[2];
        phoneNumber = "555555555";
    }

    public Pin(String n, String a, ArrayList<String> needs, double longe, double lat, String number){
        needs = new ArrayList<>();
        coords = new double[2];

        name = n;
        address = a;
        this.needs = needs;
        coords[0] = longe;
        coords[1] = lat;
        phoneNumber = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCoords(double[] coords) {
        this.coords = coords;
    }

    public void addNeed(String n){
        needs.add(n);
    }

    public void setNeeds(ArrayList<String> needs) {
        this.needs = needs;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public ArrayList<String> getNeeds() {
        return needs;
    }

    public double[] getCoords() {
        return coords;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String toString() {
        String s = "Name: "+ name + "\nNumber: " + phoneNumber + "\nAddress: " + address + "\nCoordinates: " +
                "Latitude: " + coords[0] + ", Longitude: " + coords[1] + "\nNeeds: ";
        for (int i = 0; i < needs.size(); i++) {
            s = s + needs.get(i) + ", ";
        }
        return s;
    }
}
